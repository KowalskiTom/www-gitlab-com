---
layout: handbook-page-toc
title: "ThinkBIG!"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## ThinkBIG!

The purpose of the ThinkBIG! sessions is to develop a shared understanding of goals and purpose by discussing the vision, product roadmap, user research, design, and delivery of upcoming product features. ThinkBIG! sessions aim to collect people from different organisation functions together to talk and brainstorm about the possible directions we can be taking.

### Goal
The goal of this meeting will be to align the team on our medium to long-term goals and ensure that our short-term goals are leading us. This session is a regular meeting where an entire team can discuss and understand the current vision of a product, be involved in the planning of new features, and review findings from proper user research or casual customer conversations.

### How it works
ThinkBIG meetings are scheduled once a milestone for the entire team. The agenda document is available per communication guidelines, and the video of the session is shared on our GitLabUnfiltered YouTube channel to enable asynchronous collaboration. Action items from the meeting will be turned into issues with the label like ~Package:ThinkBIG!.

### Suggested Content Modules

The future and vision of an entire stage or group can be an awkward conversation to kick off. Below are a few suggested conversation topics or discussion structures to include in your team's ThinkBIG! 

Each module has averaged around 15 minutes each, so a full session can generally cover three topics. It is suggested to have a 4th topic added to the agenda to ensure you have enough content to make the best use of time.

#### Research Review

After completing Problem or Solution Validation, Product Managers and Product Designers walk away with robust data about our customers and their needs. The ThinkBIG! session is a perfect forum for sharing the plan, process, and results of those research efforts. 

Upcoming research discussions involve the entire team and give everyone involved in creating a product or feature an opportunity to raise questions to ask the users.

Reviewing past research initiatives and discovered UX-Research Insights with the whole team helps everyone understand what our users need and how the features being built will help them.

#### Epic Overview

Reviewing a new epic with the whole team helps everyone understand the large scale vision and the logical breakdown formed. Having this conversation opens opportunities for the team to collaborate on how to break down large work into MVC deliverables in a logical and efficient order.

#### Round Robin Design Feedback

There are many ways to gather feedback in a sync-meeting. One example is to use a Round Robin turn-based structure that gives everyone on the call an opportunity to share their thoughts quickly and efficiently. 

This conversation should be timeboxed to 15 minutes, so remember to be concise and have fun with it! If you ever need inspiration for feedback, consider taking a [few different hats for a spin](https://www.mindtools.com/pages/article/newTED_07.htm)!

##### The Set-Up

The setup is straightforward. Before the sync session, prepare the agenda by pasting a link to these instructions. 

As the meeting starts, look at the attending members' names and form a randomly ordered list. This will be the order for participants to go in. Make sure to put this ordering into the agenda.

##### The Process

The designer will kick off the process by quickly reviewing the rules and starting the 15-minute timer. After the timer has started, the activity goes as follows:

1.  The designer should present the design communicating what areas they're looking for feedback on. For more genuine reactions and feedback, keep the explanation as short as possible.
1.  Following the order pasted into the agenda, participants take turns asking relevant questions and providing a single piece of feedback to the design. Each "turn" should be limited to about 1 minute.
1.  Repeat this turn-based process until time runs out or all the participants "pass."

##### The Turns

As a participant, you can do a few different things on your turn. Try to be quick, as each turn should only last around 1 minute. During your turn, you can do a few things (in order of priority):
1.  Ask questions to the designer.
1.  +1 or -1 someone else's feedback.
1.  Provide **one(1)** piece of feedback.
1.  "Pass" - you can skip your turn.
1.  You officially end your turn by calling out the name of who is next.

##### Notes about the feedback options:

*  Feedback should revolve around the areas the designer has pointed out.
*  Feedback can be either positive, negative, or neutral. Helping a designer know what **is** working is as important as what could be improved.
*  One piece of feedback can build off of another person's feedback.

Remember, the goal is to capture a quantity of specific feedback. While it may be tempting to start discussions around the design choices and feedback, this activity doesn't make for a proper forum. The designer will follow up with reviews asynchronously afterward in the issue(s) to start discussions and conversations around the feedback.

### Should this meeting be an email/issue/MR?

Having a synchronous meeting with so many individuals may feel like it goes against [GitLab's Values](https://about.gitlab.com/handbook/values/) and culture. It is essential to evaluate the ThinkBIG sessions regularly to ensure they're valuable to the team. Some questions to ask could include: 

* **[Collaboration](/handbook/values/#collaboration)** - Are the topics designed and delivered in a way that brings the team together?
* **[Results](/handbook/values/#results)** - Are the discussions leading the intended result of harmonizing the group on the long-term strategy of the stage or group?
* **[Efficiency](/handbook/values/#efficiency)** - Having an entire team on a call for an hour can be considered expensive. Are the sessions a good use of everyone's time? 
* **[Diversity & Inclusion](/handbook/values/#diversity-inclusion-and-belonging)** - Is everyone included in the conversation? Do all the participants in the session have an opportunity to discuss ideas or provide feedback?
* **[Iteration](/handbook/values/#iteration)** - Do the conversations evolve and take in feedback from the team to make the session optimal for its goal? 
* **[Transparency](/handbook/values/#transparency)** - Are the goals and outcomes of the sync discussion available to everyone after the session? 

If any of the above questions are answered with a "No," it is a sign to reevaluate the topics, discussions, and structure of ThinkBIG! and iterate to ensure the team gets the most value out of the session as possible.

