---
layout: handbook-page-toc
title: "UX Resources"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page includes information about UX Resources to help you do your job. It is not intended to be an all-inclusive list or to limit the methodologies or approaches you might take in your daily work. If you believe that something useful is missing, please submit an MR!

## UX team workflows

* [UX Department](/handbook/engineering/ux/ux-department-workflow/)
* [Product Designer workflows](/handbook/engineering/ux/ux-designer/)
* [UX Researcher workflows](/handbook/engineering/ux/ux-research/)
* [Technical Writing workflows](/handbook/engineering/ux/technical-writing/workflow/)

## Design resources

The following resources are intended primarily to help Product Designers.

### GitLab Design project

The GitLab design project is primarily used by the Product Design team to document workflows and processes. For details, please visit the project [README](https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md).

* [View the project](https://gitlab.com/gitlab-org/gitlab-design/)

### Pajamas Design System

The GitLab Design System, [Pajamas][pajamas], was developed to increase iteration speed and bring consistency to the UI through reusable and robust components. This system helps keep the application [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) and allows designers to focus their efforts on solving user needs, rather than recreating elements and reinventing solutions. It also empowers Product, Engineering, and the Community to use these defined patterns when proposing solutions.

* [Visit design.gitlab.com][pajamas]
* [View the Pajamas UI Kit on Figma](https://www.figma.com/community/file/781156790581391771)
* [View the project](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com)
* [View dev object model (video)](https://www.youtube.com/watch?v=GMCS1sBzw9I&feature=youtu.be)

### SVGs

Our SVG repository manages all GitLab SVG assets by creating an SVG sprite out of icons and optimizing SVG-based illustrations.

* [SVG Previewer](http://gitlab-org.gitlab.io/gitlab-svgs/)
* [View the project](https://gitlab.com/gitlab-org/gitlab-svgs)

### GitLab Dribbble team account

Our [Dribbble team account](https://dribbble.com/gitlab) is a collection of design work that we've produced at GitLab:

* Product feature MVCs for engaging customer feedback
* Illustration work to support company announcements
* Sharing GitLab Resources
* Iconography work for engaging customer feedback
* Asking for input on design concepts and explorations

##### Expectations around activity on Dribbble team account

* Participation is optional and is not expected from all designers.
* There are no strict guidelines. We trust you to pick the content yourself.
* Anyone can request a seat and start contributing, but we do have a limited number of licenses. Reach out to your manager, if you're interested.

### Jobs to be Done (JTBD)
We use the JTBD framework for viewing products and solutions in terms of the jobs customers are trying to achieve. It's about understanding the goals that people want to accomplish. JTBD is used throughout the design process to determine scope, validate direction, evaluate existing experiences with [UX Scorecards](/handbook/engineering/ux/ux-scorecards/), and assess our [Category Maturity](/direction/maturity/) with [Category Maturity Scorecards](/handbook/engineering/ux/category-maturity-scorecards).

* [JTBD Overview](/handbook/engineering/ux/jobs-to-be-done/)
* [JTBD Deep Dive](/handbook/engineering/ux/jobs-to-be-done/deep-dive/)
* [Core JTBD principles](/handbook/engineering/ux/jobs-to-be-done/core-jobs-to-be-done-principles/)
* [Validating JTBD](/handbook/engineering/ux/jobs-to-be-done/validating-jobs-to-be-done/)
* [Mapping JTBD](/handbook/engineering/ux/jobs-to-be-done/mapping-jobs-to-be-done/)

### Synchronous Design Reviews
Synchronous design feedback is an effective way to share and receive a constructive critique of your design work from stable counterparts and team members. We have two types: design reviews with a focus on current milestone work and [ThinkBig!](/handbook/engineering/ux/thinkbig/) sessions for vision and longer-term goals.

### Tools

**Mural** We use [Mural](https://mural.co/) for collecting design feedback, mapping workflows, brainstorming, affinity mapping, and anything else where we need a visual, whiteboard-like workspace.

Everyone in the UX department and all Product Managers can get a Mural account with the ability to create new Murals. If you want to share your Mural to get feedback from members of your team who do not have a Mural account, you can send an anonymous link via the Share dialog.

**Figma** We use [Figma](https://www.figma.com/design/) for designing and prototyping. Our [Pajamas UI kit](https://www.figma.com/file/qEddyqCrI7kPSBjGmwkZzQ/Pajamas-UI-Kit) contains design assets, components, and styles for GitLab's design system, [Pajamas](https://design.gitlab.com/). Every product designer should receive access to Figma during onboarding. If you don't have the access you need, reach out to your manager. If you are not a product designer but want View access (including the ability to leave commnets), create a free Figma account and ask your stage group designer for a link to the relevant files.

**Dovetail** We use [Dovetail](https://dovetailapp.com/) to manage and analyze research findings. If you need access, please submit an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues).

### Tutorials

The UX team is encouraged to make improvements directly in the product when they find something that is small and easy to change. 

* [Technical Tutorials for Product Designers](https://www.youtube.com/playlist?list=PL05JrBw4t0Kra6RseLWXFIXtu9UPzjzbT) - video playlist for best practices on technical topics
* [How to do UI Code Contributions](./designers-guide-to-contributing-ui-changes-in-gitlab/)
* [Step by Step Guide to Make Code Changes](./designers-step-by-step-guide-to-make-code-changes/)

### Prototypes

Coming soon.

## Research resources

The following resources may be helpful to UX Researchers, Product Designers, and Product Managers, since all of these roles conduct user research. For more information, see the [UX Research](https://about.gitlab.com/handbook/engineering/ux/ux-research/#remote-design-sprint) section of the handbook.

### UX Research project

The UX Research project contains all research undertaken by GitLab's UX researchers and is only used for the organization and tracking of UX research issues.

* [View the project](https://gitlab.com/gitlab-org/ux-research)

### System usability score

Once each quarter, we run a [System Usability Scale (SUS)](https://www.usability.gov/how-to-and-tools/methods/system-usability-scale.html) survey to measure user perception of the GitLab product. We send the survey to members of the wider GitLab community, with the goal of asking for a response from any individual no more than twice per year.

* [SUS results by quarter](https://about.gitlab.com/handbook/engineering/ux/performance-indicators/#perception-of-system-usability)

### GitLab First Look

At GitLab, we want everyone to be able to contribute. To that end we created First Look where we accept applicants to participate in various studies and testing.

* [Visit GitLab First Look](/community/gitlab-first-look/index.html)

## UX performance indicators

We track our effectiveness through [UX performance indicators](https://about.gitlab.com/handbook/engineering/ux/performance-indicators/) (PIs). Our e-group reviews these PIs once a month in the Engineering Key Review.

## User personas

User personas represent the people who actually use GitLab. The UX and Marketing teams use personas to inform decisions around the user experience and design.

* [View our user personas](/handbook/marketing/product-marketing/roles-personas/index.html#user-personas)

## From the GitLab team

Not only do our team members create great work for the wider GitLab community, but they also create some amazing industry-related resources to push our craft forward.

* [Building Design Systems: Unify User Experiences through a Shared Design Language](https://www.amazon.com/Building-Design-Systems-Experiences-Language/dp/148424513X), by Taurie Davis and Sarrah Vesselov
* [Craft Awesome Web Typography](https://betterwebtype.com/web-typography-book/), by Matej Latin
* [Hemingway - Figma plugin](https://www.figma.com/community/plugin/760035865558407437/Hemingway), by Michael Le
* [GitLab-ipsum](https://ipsum.reali.sh/), by Patrick Deuley and Jeremy Elder

[pajamas]: https://design.gitlab.com/
