---
layout: markdown_page
title: "Meghan Maneval's README"
---

## Meghan Maneval's README

**[Meghan Maneval- Manager, Risk and Field Security.]** This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Related pages

* GitLab Profile: [Meghan Maneval · GitLab](https://gitlab.com/mmaneval20)
* LinkedIn Profile: [Meghan Dobson-Maneval](https://www.linkedin.com/in/meghanmaneval/)

# About Me

* My original major in college was actually Interior Design and Architecture.  However, shortly after my first semester, I decided to change my major to Management of Technology.
* My first job after graduation was an IT Auditor for an insurance company.
* I moved to Arizona from Boston in 2008 and pursued my Master’s Degree in Business. 
* In 2016, I accepted a role at a global language services company to build out their Information Security Management System and Enterprise Risk Management program with the goal of ISO 27001 Certification. 
* I am a mother to 5 amazing children from age 9 to 19. We live in a  [small town](https://www.maranaaz.gov/) between Tucson and Phoenix Arizona. 
* My hobbies include: 
	* Reading- anything from Jane Austin to JK Rowling and everything in between. 
	* Anything outdoors (which may seem odd when our temperatures get above 115* F in the summers). I love camping, hiking, swimming, and general exploring. 
	* Interior design and furniture restoration
	* Video editing and movie making
* I volunteer for the [Girl Scouts of Southern Arizona](https://www.girlscoutssoaz.org/), [The Arthritis Foundation](https://www.arthritis.org/home), [Drama Kids International](https://dramakids.com/tuscon-oro-valley-catalina-foothills-az/) and [ISACA](https://www.isaca.org/). In particular, I have spoken and am an advocate for Women in Technology. 
* My guilty pleasures are TV Home Remodeling shows (who doesn’t love Chip and Joanne), 2000’s Pop Music (yes, that includes Brittney) and Buffalo Wings (I could eat them every meal). 

One of the things that drew me to GitLab was the clearly documented values. I am a huge proponent of [Negative Feedback](https://about.gitlab.com/handbook/values/) being done one on one and [Assuming Positive Intent](https://about.gitlab.com/handbook/values/).  I believe that most of life’s problems can be resolved before they become major if people would simply talk to each other, admit when they are wrong, and collaboratively come up with a solution that meets both parties’s needs.  

# How you can help me
* I am a “list” person- which makes GitLab issues the perfect way to communicate tasks and requests with me. However, given my role I will tend to get requests from all areas. If the matter is urgent, I would ask that you follow up with an email to let me know the urgency and expected due date. This will help me prioritize my lists. 
* I am human and I will make mistakes. If that mistake negatively impacts you, I want to know. I can’t improve unless I know where I went wrong.
* Let me know your preferred work style. And if my style is not meshing with your’s, please tell me. I will do my best to meet you in the middle. 

# My working style
* As noted above, I work best off of lists that I can break down to task levels and set priorities and due dates. If you need me to complete something or assist with something, please be sure to include a due date, requirements and any pertinent information. 
* I prefer direct and to the point communication. Don’t worry if you are blunt, I won’t take this personal. 

# What I assume about others
* I assume that if a due date is provided, the due date will be met. If there is risk to competing something on time, I expect that party to prepare an alternate timeline and a plan to meet the updated due date. 
* I assume that if others have questions for me, they will reach out. If they don’t ask for help or clarification, I assume they have all the information to complete the task.  
* I assume that others are open and honest and won’t hide things from me. 

# What I want to earn
* Experience and knowledge of the DevOps world and GitLab products and services
* A sense of collaboration with other Risk and Security Subject Matter Experts
 
# Communicating with me
* I prefer face to face. Slack is ok for quick things, but I prefer scheduled Zoom meetings for larger items so I can dedicate the time to that topic specifically. 
* I generally begin my day around 7-7:30 am PST and tend to end my day mid-afternoon. 
* I don’t plan on traveling, however, I am very diligent about using Auto-Reply Out of Office Messages when I will be away for extended periods of time. 

