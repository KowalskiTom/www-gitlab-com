---
layout: markdown_page
title: "Security Assurance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Assure the Customer

The mission of the security assurance sub-department is to provide assurance to GitLab customers that any data shared with GitLab will be kept safe and our customer's privacy will be respected.

## Teams within Security Assurance

* [Field Security](/handbook/engineering/security/security-assurance/field-security/)
* [Security Compliance](/handbook/engineering/security/security-assurance/security-compliance/compliance.html)

## Stable Counterparts

The idea behind stable counterparts is articulated best in [related infrastructure handbook page](https://about.gitlab.com/handbook/engineering/infrastructure/library/organization/stable-counterparts/).

### Security Compliance Counterparts

| GitLab Team | Primary SecComp counterpart |
| :---: | :---: |
| [Infrastructure](/handbook/engineering/infrastructure/) | [@nsarosy](https://gitlab.com/nsarosy) |
| [SecOps](/handbook/engineering/security/operations/) | [@lcoleman](https://gitlab.com/lcoleman) |
| [AppSec](/handbook/engineering/security/application-security/) | [@twsilveira](https://gitlab.com/twsilveira) |
| [BizOps](/handbook/business-ops/) | [@uswaminathan](https://gitlab.com/uswaminathan) |
| [Team Member Enablement](/handbook/business-ops/team-member-enablement/) | [@uswaminathan](https://gitlab.com/uswaminathan) |
| [Internal Audit](/handbook/internal-audit/) | [@twsilveira](https://gitlab.com/twsilveira) |
| [Data Team](/handbook/business-ops/data-team/) | [@sttruong](https://gitlab.com/sttruong)  |
| [People team](/handbook/people-group/) | [@jblanco2](https://gitlab.com/jblanco2) |
| [Security Automation](/handbook/engineering/security/automation/) | [@nsarosy](https://gitlab.com/nsarosy) |

## How to contact us

Join our slack channel: #sec-assurance