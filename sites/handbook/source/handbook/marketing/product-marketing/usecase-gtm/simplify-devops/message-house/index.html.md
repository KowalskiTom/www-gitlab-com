---
layout: markdown_page
title: "Simplify DevOps Message House"
---



| Positioning Statement: | *(how GitLab fits and is differentiated in the market for this use case)* |
|------------------------|-------------------------------------------------------------------------|
| **Short Description** | *25 words or less* |
| **Long Description** | *100 words or less* |


| **Key-Values** | Value 1: *<List a key message/ value proposition>* | Value 2: | Value 3: |
|--------------|------------------------------------------------------------------|----------|----------|
| **Promise** | *(list and describe the positive business outcomes)* |  |  |
| **Pain points** | *(describe common pain points)* |  |  |
| **Why GitLab** | *(list specific features that support this value)* |  |  |


| **Proof points** | *(list specific analyst reports, case studies, testimonials, etc)*  |
