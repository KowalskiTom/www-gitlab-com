---
layout: handbook-page-toc
title: "Industry Analyst Briefings at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The purpose of a briefing is to inform or educate analysts about something we are doing. The majority of the time, GitLab is expected to be presenting information, usually through presentations and demos, along with some Q&A time for analysts to get additional clarity or further explore a topic. Some industry analyst firms, like Gartner, [will generally not provide feedback in a briefing](https://www.gartner.com/en/contact/vendor-briefings) and will prefer to do that in a follow-on inquiry as their feedback and advice is a core component of their commercial service offering. There are also other analyst firms that are more than happy to provide feedback they might have during the briefing itself.

In either case, your [GitLab Industry Analyst Relations team](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/#which-analyst-relations-team-member-should-i-contact) is ready to answer any questions you might have and if you would like to brief an analyst please [click here](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=AR-ResearchRequest) and fill out this request form.


## Types of briefings
There are several different, yet common types of briefings that the industry analyst relations team will organize and help you deliver. These include:

### Announcement briefings 
These are briefings intended to familiarize analysts with an event or one-time change. They are usually done before the date of the announcement itself and may also prepare analysts to speak to their clients (end users, buyers, investors, etc.) and/or press about the announcement. Examples of announcement briefings include, but are not limited to:
-  Acquisition
-  Price or GitLab-level product change
-  New executive
-  Technology partner announcement
    

### Use case or solution updates
These are ongoing, approximately bi-annually sharing updates on GitLab's go-to-market approach through use cases. These briefings have a similar structure across use cases with shared content as well as individual use case content. Updates to information initially shared in an annoucement briefing should be incorporated into appropriate use case briefings for future briefings. 

### Product section or release updates
These are briefings that focus on one or more sections or stages. Not all our product areas fit within a use case, so this is another way to share what we're doing. These may also be used for product release updates done on a quarterly or annual basis.

### Analyst-requested briefings
These briefings are generally set up in response to an inbound analyst request e.g. in support of comparative research or to inform an analyst's response to a client inquiry they received.
   
## Briefing mechanics
- Briefings can last 30 or 60 minutes minutes, at the analysts' discretion, and are usually conducted by videoconference. IAR will either schedule a Zoom call or the analyst will provide a link to their videoconference software. Gartner and Forrester tend to use Webex. IDC, 451 and Redmonk are all generally available for a GitLab Zoom.
- In contrast to analyst inquiries, which are an entitlement of research seats purchased, briefings are available to clients and non-clients alike in an effort for analyst firms to maintain both the quality of their advice and their objectivity. As such, analysts will tend to accept briefing requests when the topic aligns with their research coverage area(s), but have the option of politely declining if the topic is not aligned with their client needs or interest.
- Briefings can include multiple analysts from the same firm, as schedules allow, or scheduled over several calls to accommodate different time zones, focus areas, etc.
- Generally speaking, analysts (especially from Forrester and Gartner) will not offer feedback during briefings, with firm policy requiring GitLab to schedule inquiry calls in follow-up to get feedback and advice on positioning, strategy, etc.

