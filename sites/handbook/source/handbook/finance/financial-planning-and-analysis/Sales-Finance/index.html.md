---
layout: handbook-page-toc
title: "Sales Finance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Sales Finance Handbook!

## Common Links

  * [Professional Services (PS) Finance](/handbook/finance/financial-planning-and-analysis/Sales-Finance/Professional-Services-Finance)

### Finance Business Partner Alignment

| Name | Function |
| -------- | ---- |
| Frederick | All |
| Alexandria | Enterprise Sales, Channels, Alliances |
| Yi | Commercial Sales, Customer Success, Professional Services, Field Ops |

## Sales Forecast Rhythm
We believe an excellent forecasting process enables us to deploy our resources effectively, risk-manage the business, and provide early warning systems. At GitLab, we design our Sales Forecast Rhythm to foster careful inspection and execution of bookings target throughout the quarter. Each week we review various aspects of the business, such as Current/Next Quarter pipeline, Renewals timing, and leading indicators KPIs, to name but a few.

<figure class="video_container">
<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vStHiw-vsSJXuWFkB-iZ37wZXI1GXdI1bQpTin5TfU6v1_PWMFgSjxuttzgCqUeucagCiLPjJAmOKkq/pubhtml?widget=true&amp;headers=false"></iframe>
</figure>

## Pipeline Velocity
[File](https://docs.google.com/spreadsheets/d/1O81k_XpInMqn_pLPdbPJruh_nq0QQthJ9-nOiXZzBf0/edit#gid=2079563316)

## Net IACV Pipeline Movement
[File](https://docs.google.com/spreadsheets/d/1L4Rl6hGb5t8x8f_3ILwUlYvOL7Rdo2Rh8FZf0RumBqE/edit#gid=1993507993)
