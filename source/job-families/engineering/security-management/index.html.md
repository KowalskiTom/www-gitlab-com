---
layout: job_family_page
title: "Security Engineering Management"
---

## Security Management Roles at GitLab

Managers in the security engineering department at GitLab see the team as their product. While they are technically credible and know the details of what security engineers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of security commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### Manager Level Core Competencies

##### Action Oriented
Enjoys working hard; is action oriented and full of energy for the things he/she sees as challenging; not fearful of acting with a minimum of planning; seizes more opportunities than others.

##### Dealing With Ambiguity
Can effectively cope with change; can shift gears comfortably; can decide and act without having the total picture; isn’t upset when things are up in the air; doesn’t have to finish things before moving on; can comfortably handle risk and uncertainty.

##### Business Acumen
Knows how businesses work; knowledgeable in current and possible future policies, practices, trends, and information affecting their business and organization; knows the competition; is aware of how strategies and tactics work in the marketplace.

##### Confronting Direct Reports
Deals with problem direct reports firmly and in a timely manner; doesn’t allow problems to fester; regularly reviews performance and holds timely discussions; can make negative decisions when all other efforts fail; deals effectively with troublemakers.

##### Decision Quality
Makes good decisions (without considering how much time it takes) based upon a mixture of analysis, wisdom, experience, and judgment; most of their solutions and suggestions turn out to be correct and accurate when judged over time; sought out by others for advice and solutions.

##### Developing Staff
Provides challenging and stretching tasks and assignments; holds frequent development discussions; is aware of each direct report’s career goals; constructs compelling development plans and executes them; pushes direct reports to accept developmental moves; will take direct reports who need work; is a people builder.

##### Directing Others
Is good at establishing clear directions; sets stretching objectives; distributes the workload appropriately; lays out work in a well-planned and organized manner; maintains two-way dialogue with others on work and results; brings out the best in people; is a clear communicator.

##### Integrity and Trust
Is widely trusted; is seen as a direct, truthful individual; can present the unvarnished truth in an appropriate and helpful manner; keeps confidences; admits mistakes; doesn’t misrepresent him/herself for personal gain.

##### Managerial Courage
Doesn’t hold back anything that needs to be said; provides current, direct, complete, and “actionable” positive and corrective feedback to others; lets people know where they stand; faces up to people problems on any person or situation (not including direct reports) quickly and directly; is not afraid to take negative action when necessary.

##### Managing and Measuring Work Clearly
assigns responsibility for tasks and decisions; sets clear objectives and measures; monitors process, progress, and results; designs feedback loops into work.

##### Motivating Others
Creates a climate in which people want to do their best; can motivate many kinds of direct reports and team or project members; can assess each persons hot button and use it to get the best out of him/her; pushes tasks and decisions down; empowers others; invites input from each person and shares ownership and visibility; makes each individual feel their work is important; is someone people like working for and with.

##### Planning Accurately
scopes out length and difficulty of tasks and projects; sets objectives and goals; breaks down work into the process steps; develops schedules and task/people assignments; anticipates and adjusts for problems and roadblocks; measures performance against goals; evaluates results.

##### Problem Solving
Uses rigorous logic and methods to solve difficult problems with effective solutions; probes all fruitful sources for answers; can see hidden problems; is excellent at honest analysis; looks beyond the obvious and doesn’t stop at the first answers.

##### Drive For Results
Can be counted on to exceed goals successfully; is constantly and consistently one of the top performers; very bottom-line oriented; steadfastly pushes self and others for results.


### Manager, Security Engineering

* Hire a world class team of security engineers and analysts to work on their team
* Help their team grow their skills and experience
* Provide input on security architecture, issues, and features
* Hold regular 1:1's with all members of their team
* Create a sense of psychological safety on their team
* Recommend security-related technical and process improvements
* Author project plans for security initiatives
* Draft quarterly OKRs
* Train team members to screen candidates and conduct managerial interviews
* Strong sense of ownership, urgency, and drive
* Excellent written and verbal communication skills, especially experience with executive-level communications
* Ability to make concrete progress in the face of ambiguity and imperfect knowledge
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)

### Job Grade

The Manager, Security Engineering is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Field Security

* The same responsibilities of a Security Manager, plus the below:
* Build a strong, collaborative partnership with sales and product teams
* Participate on customer calls providing assurance on GitLab security
* Create innovative and effective strategies for addressing customer requests
* Assess and promote customer security requests across the GitLab security and product teams

### Director Level Core Competencies

##### Command Skills
Relishes leading; takes unpopular stands if necessary; encourages direct and tough debate but isn’t afraid to end and move on; is looked to for direction in a crisis; faces adversity head on; energized by tough challenges.

##### Conflict Management
Steps up to conflicts, seeing them as opportunities; reads situations quickly; good at focused listening; can hammer out tough agreements and settle disputes equitably; can find common ground and get cooperation with minimum noise.

##### Perspective
Looks toward the broadest possible view of an issue/challenge; has broad-ranging personal and business interests and pursuits; can easily pose future scenarios; can think globally; can discuss multiple aspects and impacts of issues and project them into the future.

##### Presentation Skills
Is effective in a variety of formal presentation settings: one-on-one, small and large groups, with peers, direct reports, and bosses; is effective both inside and outside the organization, on both cool data and hot and controversial topics; commands attention and can manage group processes during the presentation; can change tactics midstream when something isn’t working.

##### Priority Setting
Spends his/her time and the time of others on what’s important; quickly zeros in on the critical few and puts the trivial many aside; can quickly sense what will help or hinder accomplishing a goal; eliminates roadblocks; creates focus.

##### Process Management
Good at figuring out the processes necessary to get things done; knows how to organize people and activities; understands how to separate and combine tasks into efficient work flow; knows what to measure and how to measure it; can see opportunities for synergy and integration where others can’t; can simplify complex processes; gets more out of fewer resources.

##### Strategic Agility
Sees ahead clearly; can anticipate future consequences and trends accurately; has broad knowledge and perspective; is future oriented; can articulately paint credible pictures and visions of possibilities and likelihoods; can create competitive and breakthrough strategies and plans.

##### Building Effective Teams
Blends people into teams when needed; creates strong morale and spirit in their team; shares wins and successes; fosters open dialogue; lets people finish and be responsible for their work; defines success in terms of the whole team; creates a feeling of belonging in the team.

##### Managing Vision and Purpose
Communicates a compelling and inspired vision or sense of core purpose; talks beyond today; talks about possibilities; is optimistic; creates mileposts and symbols to rally support behind the vision; makes the vision shareable by everyone; can inspire and motivate entire units or organizations.

### Director of Security

The Director of Security role extends the [Security Manager](#security-engineering-manager) role.

* Own a Sub-department of the GitLab Security Department
* Run multiple teams within their Sub-department.
* Hire a world class team of managers and security engineers to work on their teams
* Assess and mitigate constantly changing threats
* Help managers and individual contributors grow their skills and experience
* Manage multiple teams and projects
* Hold weekly 1:1s with their direct reports
* Hold monthly skip-level 1:1's with all members of their team
* Create a sense of psychological safety on their Sub-department
* Drive technical and process improvements
* Drive quarterly OKRs
* Represent the company publicly at conferences

##%# Job Grade

The Director, Security Engineering is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Secure our product, services (GitLab.com, package servers, other infrastructure), and company (laptops, email)
- Keep our risk analysis up to date
- Define and plan priorities for security related activities based on that risk analysis
- Determine appropriate combination of internal security efforts and external
security efforts including bug bounty programs, external security audits
(penetration testing, black box, white box testing)
- Analyze and advise on new security technologies
- Build and manage a team, which currently consists of [Security Managers](/job-families/engineering/security-management), [Security Engineers](/job-families/engineering/security-engineer), and [Security Analysts](/job-families/engineering/security-analyst)
   - Identify and fill positions
   - Grow skills in team leads and individual contributors, for example by
   creating training and testing materials
   - Deliver input on promotions, function changes, demotions, and terminations
- Ensure our engineers and contributors from the wider community run a secure software development lifecycle for GitLab by training them in best practices and creating automated tools
- Involve in major security and service abuse events
- Ensure we're compliant with our legal and contractual security obligations
- Evangelise GitLab Security and Values to staff, customers and prospects

#### Requirements

- Significant application and SaaS security experience in production-level settings
- This position does not require extensive development experience but the
candidate should be very familiar with common security libraries, security
controls, and common security flaws that apply to Ruby on Rails applications
- Experience managing teams of engineers, and leading managers
- Experience with incident management
- You share our [values](/handbook/values), and work in accordance with those values
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)
- Ability to use GitLab

### VP of Security

### Job Grade

The VP of Security is a [grade 12](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Set the vision of the Gitlab Security Department with a clear roadmap
* Build and maintain a rapidly growing team with top-tier talent
* Run the most transparent security organization in the world
* Establish and implement security policies, procedures, standards, and guidelines
* External communications: Blog, conference speaking, stream company events to YouTube
* Work directly with customers and prospects to address security concerns
* Manage a best-in-class bug bounty program with the highest rewards
* Maintain Investor relations with regard to security
* Act as central point-of-contact to Facility Security Officer for cleared facilities
* Collaborate closely with People Ops, Legal, and any third-party firms to ensure the health and safety of organization’s employees globally
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)

#### Must-haves Skills & Experience

GitLab’s senior director of Security must have all of the following attributes.

* At least 10 years prior experience managing information security teams
* Excellent written and verbal communication skills
* Be able to quickly hire top-quality individuals contributors and managers
* Experience managing a multi-level security organization with managers and IC’s
* Collaborate with other groups outside engineering such as Sales, Legal, People Ops, and Finance
* Ability to excel in a remote-only, multicultural, distributed environment
* Possess domain knowledge of common information security management frameworks and regulatory requirements and applicable standards such as ISO 27001, SOC 2, HIPAA, GDPR, PCI, Sarbox, etc.
* Excellent project and program management skills and techniques

#### Nice-to-have Skills & Experience

Great candidates will have some meaningful proportion of the following.

* Working knowledge of the GitLab application
* Relevant Bachelor's degree
* Prior fast-growing startup experience
* US Government security clearance
* Product/Platform company experience
* Self-managed (on-prem) software experience
* SaaS software experience
* Experience with consumer-scale services
* Developer platform/tool industry experience
* Deep open source software (OSS) experience

## Performance Review Experiment in Security Department

Performance reviews are a formal method of providing feedback to employees. A good performance review is designed to facilitate conversations between employees and their managers.  The benefits of performance reviews include:

* Transparency
* Recognition
* Improve communication
* Promotion and Compensation support
* Employee Development
* Career goals and path

Currently, there exists a company-wide [360 performance review process](https://about.gitlab.com/handbook/people-group/360-feedback/) and an optional annual [compa group spreadsheet](https://docs.google.com/spreadsheets/d/1Se66ZXrRKetMXEPOM2kRptiupCYLzwJrzpRxs0ITP4g/edit#gid=395345394) run by the People Group.

The Security Department is adding to that process to try and deliver additional value to team members and the company. This performance review cycle will be 6 months. The specific objecties we intend to achieve with the experiment are:

* Recognition of team member's achieveents over a specified period of time
* Promote transparency by providing a clear understanding of assessed performance
* Provide clear understanding career growth and opportunities to achieve next level
* Source of truth to support promotions and pay raises
* Influence and impact positive improvement across weak and underporforming areas

We'll assess wether this criteria was achieved at the end of the 6 month period with a Security Department member survey, and a review with their People Operations business partner. Upon success, parts or the whole of this experiment will be integrated into the company-wide review process. Also, we may decide to continue the experiment, or perform a new one.

The framework of the performance review is as follows:

* Overview and Accomplishments
* GitLab Values
* Role Responsibilities and expectations
* Career Path
* Next Steps (short term improvements)
* Employee Feedback

The performance review process is as follows:

* Managers draft reviews of direct reports
* Each performance review will be critiqued by at least 2 others in security management (manager, director, vp)
* Managers will make final edits of performance review and deliver to direct report
* A 1:1 will be scheduled to faciliate conversation and feedback
* Individuals feedback will be included in the performance review
* A post-cycle review session will be held with security management and people business partner to review the process, what went well, areas for improvement
* Managers will continue to track progress and maintain conversation through 1:1's

## Performance Indicators

Security Management has the following job-family performance indicators.

* [Hiring actual vs plan](/handbook/engineering/security/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/security/performance-indicators/#diversity)
* [Handbook update frequency](/handbook/engineering/security/performance-indicators/#handbook-update-frequency)
* [Team member retention](/handbook/engineering/security/performance-indicators/#team-member-retention)
* [HackerOne spend actual vs planned](/handbook/engineering/security/performance-indicators/#hackerone-budget-vs-actual-with-forecast-of-unknowns)

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.
