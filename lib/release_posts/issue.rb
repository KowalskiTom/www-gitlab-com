# frozen_string_literal: true

require 'base64'
require 'uri'

require_relative '../changelog/api_retry'

module ReleasePosts
  class Issue
    include ApiRetry
    include Helpers

    PROBLEM_REGEX = /^[#]+ Problem to solve\W*^(?<description>.*?)\s*^#/im
    IMAGE_REGEX = /\!\[mockup\]\((?<image>.*?)\)/im

    def initialize(issue, type_name)
      @issue = issue
      @type_name = type_name
    end

    def iid
      @issue['iid']
    end

    def project_id
      @issue['project_id']
    end

    def title
      @issue['title']
    end

    def description
      @issue['description']
    end

    def labels
      @issue['labels']
    end

    def milestone_id
      @issue['milestone']['iid']
    end

    def milestone_title
      @issue['milestone']['title']
    end

    def web_url
      @issue['web_url']
    end

    def group_label
      labels.find { |label| label.start_with? GROUP_LABEL_PREFIX }
    end

    def group
      group_label.delete_prefix(GROUP_LABEL_PREFIX)
    end

    def stage_label
      labels.find { |label| label.start_with? STAGE_LABEL_PREFIX }
    end

    def stage
      stage_label.delete_prefix(STAGE_LABEL_PREFIX)
    end

    def categories
      category_labels = labels.select { |label| label.start_with? CATEGORY_LABEL_PREFIX }
      category_labels.map { |label| titleize(label.delete_prefix(CATEGORY_LABEL_PREFIX)) }
    end

    def available_in
      available_in = ['ultimate']
      return available_in if labels.include? 'GitLab Ultimate'

      available_in.unshift('premium')
      return available_in if labels.include? 'GitLab Premium'

      available_in.unshift('starter')
      return available_in if labels.include? 'GitLab Starter'

      available_in.unshift('core')
    end

    def release_post_type_label
      labels.find { |label| label.start_with? RP_LABEL_PREFIX }
    end

    def type_name
      return @type_name if @type_name

      type_found = TYPES.find do |type|
        type.label == release_post_type_label
      end

      type_found ? type_found.name : nil
    end

    def slug
      slugify("#{stage}_#{group}_#{title}")
    end

    def problem_to_solve
      if matches = description.match(PROBLEM_REGEX)
        matches[:description]
      else
        "Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit. Perferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.\n"
      end
    end

    def image_url
      return unless matches = description.match(IMAGE_REGEX)

      matches[:image].to_s
    end

    def image_binary
      u = URI(web_url.split('/-/')[0])
      u.path += image_url

      URI.open(u).read
    end

    def image_base64
      Base64.strict_encode64(image_binary)
    end

    def release_post_workflow_label
      labels.find { |label| label.start_with? RELEASE_POST_WORKFLOW_PREFIX }
    end

    def add_label(label)
      api_retry do
        Gitlab.edit_issue(project_id, iid, add_labels: [label])
      end
    end

    private

    def titleize(string)
      string.split.each(&:capitalize!).join(' ').to_s
    end

    def slugify(value)
      value.strip.gsub(/\s/, '_').gsub(/\W/, '').downcase
    end
  end
end
